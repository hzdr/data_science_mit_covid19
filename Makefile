
SRC_FILES=$(wildcard *Rmd)

PDF_FILES=$(SRC_FILES:%.Rmd=%.pdf)
HTML_FILES=data_science_mit_covid19.html #$(SRC_FILES:%.Rmd=%.html)

all : $(HTML_FILES)

%.html : %.Rmd
	@Rscript -e "rmarkdown::render('$<',output_format='html_document')"

%.pdf : %.Rmd
	@Rscript -e "rmarkdown::render('$<',output_format='pdf_document')"

%.docx : %.Rmd
	@Rscript -e "rmarkdown::render('$<',output_format='word_document')"

zip: data_science_mit_covid19.html data_science_mit_covid19.docx
	@rm *.zip
	@zip data_science_mit_covid19.zip $^ *svg

clean :
		rm -fv $(HTML_FILES)

echo :
	@echo ${HTML_FILES}
