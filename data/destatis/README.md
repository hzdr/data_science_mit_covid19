# mortality tables / Sterbetafeln

## `12621-0001.csv`

source: https://www-genesis.destatis.de/genesis//online?operation=table&code=12621-0001&levelindex=0&levelid=1586450535990

q(x) = Sterbewahrscheinlichkeit vom Alter x bis x+1
p(x) = Überlebenswahrscheinlichkeit vom Alter x bis x+1
l(x) = Überlebende im Alter x
d(x) = Gestorbene im Alter x bis unter x+1
L(x) = Von den Überlebenden im Alter x bis zum Alter x+1
       durchlebte Jahre
T(x) = Von den Überlebenden im Alter x insgesamt noch
       zu durchlebende Jahre
e(x) = Durchschnittliche Lebenserwartung (von den
       Überlebenden) im Alter x in Jahren


# Addendum

Ähnliche Datensätze für andere Länder.

SUI: https://discourse.data-against-covid.org/t/request-for-data-confirmed-cases-by-gender-and-5-year-age-groups/657/4?u=psteinb

PRC, ITA, COREA, SPA: https://discourse.data-against-covid.org/t/request-for-data-confirmed-cases-by-gender-and-5-year-age-groups/657/5?u=psteinb

GER, CAN: https://discourse.data-against-covid.org/t/number-of-deaths-per-age-group-per-country/940/2?u=psteinb
