# RKI Daten zu COVID19 Diagnosen in Deutschland

## Abhängigkeiten

- `covid19germany`, zur Installation ist folgendes nötig:  
``` shell
$ R
> install.packages('remotes')
# a lot of output
> remotes::install_github("nevrome/covid19germany")
```

- `tidyverse`, dafür bitte folgendes ausführen:  
``` shell
$ R
> install.packages(c("tidyverse", "Rmarkdown"))
# a lot of output
```

## Download der Daten

Das wird in [download_data.R](download_data.R) gemacht oder mit dem Makefile:

``` shell
$ make
```
