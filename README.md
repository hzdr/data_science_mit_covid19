# Blogpost

Dieses Repositorium beinhaltet den Code, auf dem der Blogpost "https://..." aufbaut. Im Sinne des OpenScience- und OpenSource-Gedanken soll dieses Repo die Reproduzierbarkeit und Transparenz des Artikels unterstützen. Der Blogpost selbst ist in der Datei [data_science_mit_covid19.Rmd](./data_science_mit_covid19.Rmd) zu finden.

## Abhängigkeiten und Software

Um den Blogpost zu bauen, muss eine funktionierende [R installation](https://www.r-project.org/) vorhanden sein. 

Dann werden noch 2 Pakete gebraucht:

``` shell
$ R
> install.packages(c("tidyverse", "Rmarkdown"))
# alot of output
```

## Den Blogpost bauen

im Wurzelverzeichnis des Post, führen Sie bitte folgendes auf der Kommandozeile aus:

``` shell
$ make
Rscript -e "rmarkdown::render('data_science_mit_covid19.Rmd',output_format='html_document')"
#...output...
```

Das wird eine `.html`-Daten mit Namen `data_science_mit_covid19.html` produzieren, die man sich mit einem beliebigen Browser anschauen kann.

``` shell
$ firefox ./data_science_mit_covid19.html
```

Wem eine `.pdf`-Datei besser gefällt, der macht:

``` shell
$ make data_science_mit_covid19.pdf
Rscript -e "rmarkdown::render('data_science_mit_covid19.Rmd',output_format='pdf_document')"
#...output
```

Das produziert `data_science_mit_covid19.pdf` im aktuellen Verzeichnis.

## Disclaimer

Wer Fragen zu oder Probleme mit dem Code hat, beschreibt diese bitte in einem Issue. 

Achtung: Der Code wird unter Linux entwickelt. Ich kann versuchen Problem auf Windows oder MacOS zu lösen, verspreche aber keinen Erfolg, da ich weder noch zu meiner freien Verfügung habe.

## Lizenz

Der Blogpost steht unter [CC-BY 4.0](./LICENSE).
